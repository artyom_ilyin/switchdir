# if disks are different then
# check available space for both
# if the smallest file on disk is bigger than available space on the second disk then
# if the smallest file on the second disk is bigger than available space on the first disk then
# complain

# !! icon, title, rename folder

import os
import shutil
import threading
import sys

import wx
from hurry.filesize import size


APP_TITLE = "Ultra Mega Moving Machine Ultimate Extended Edition ArkhiPro"
CHOOSE_DIRS = "Выбери папки, которые нужно перекинуть"
CHOOSE_DIR1_BUTTON = "Выбрать папку 1"
CHOOSE_DIR2_BUTTON = "Выбрать папку 2"
SWITCH_BUTTON = "Заменить"
SAME_DRIVE = "Это один и тот же диск. Вы зачем меня вообще позвали?"
ALL_SET = "Тэк, ну вроде все ок, можно заменять."
DRIVE_ROOT = "Сам диск выбрать нельзя. Создай папку и перемести в нее все нужное добро."
CHECKING_THE_DRIVE = "Чекаем диск..."
SELECT_BOTH_DIRS = "Нужно выбрать обе папки"
IN_PROGRESS = "Работаем... Не дыши!"
ERROR_MESSAGE = "Никогда такого не было и вот опять..."
ERROR_STATUS = "Произошел троллинг"


class MovingThread(threading.Thread):
	def __init__(self, parent, dir_obj1, dir_obj2):
		super(MovingThread, self).__init__()
		self._parent = parent
		self.dir_obj1 = dir_obj1
		self.dir_obj2 = dir_obj2

	def run(self):
		self._parent.move_dat_shit()
		evt = FileMovedEvent(file_moved_event_type, -1, self.dir_obj1, self.dir_obj2)
		wx.PostEvent(self._parent, evt)


class FileMovedEvent(wx.PyCommandEvent):
	def __init__(self, etype, eid, dir_obj1, dir_obj2):
		super(FileMovedEvent, self).__init__(etype, eid)
		self.dir_obj1 = dir_obj1
		self.dir_obj2 = dir_obj2

	def get_value(self):
		return self.dir_obj1, self.dir_obj2


class Directory:
	@staticmethod
	def get_size_and_fileset(start_path):
		total_size = 0
		fileset = []
		for dirpath, dirnames, filenames in os.walk(start_path):
			for f in filenames:
				fp = os.path.join(dirpath, f)
				fileset += [os.path.relpath(fp, start_path)]
				total_size += os.path.getsize(fp)
		return total_size, fileset

	def get_drive_info(self):
		self.drive = os.path.splitdrive(self.path)[0]
		self.total, self.used, self.free = shutil.disk_usage(self.drive)
		used_pct = round(self.used / self.total * 100)
		free_pct = round(self.free / self.total * 100)
		total = size(self.total)
		used = size(self.used)
		free = size(self.free)
		self.dir_size, self.fileset = self.get_size_and_fileset(self.path)
		self.output_dir = f"From_Drive_{self.drive[:-1]}_({os.path.basename(self.path)})"

		self.files_num = len(self.fileset)
		first_line = f"Папка: {self.path}"
		second_line = f"Диск: {self.drive[:-1]}, Размер папки: {size(self.dir_size)}, Файлов: {self.files_num}"
		last_line = f"Всего: {total}, Занято: {used} ({used_pct}%), Свободно: {free} ({free_pct}%)"
		return first_line, second_line, last_line

	def __init__(self, num):
		self.number = num
		self.path = None
		self.drive = None
		self.total = None
		self.used = None
		self.free = None
		self.fileset = None
		self.dir_size = None
		self.files_num = None
		self.files_done_num = 0
		self.output_dir = None
		self.full_output_dir = None
		self.done = False


class MovePanel(wx.Panel):

	def select_dir(self, event, dir_obj, label_num):
		with wx.DirDialog(self, CHOOSE_DIR1_BUTTON, style=wx.DD_DIR_MUST_EXIST) as directoryDialog:
			if directoryDialog.ShowModal() != wx.ID_CANCEL:
				path = directoryDialog.GetPath()
				if os.path.splitdrive(path)[1] == '\\':
					self.status_bar.SetStatusText(DRIVE_ROOT)
					self.move_button.Disable()
					return
				dir_obj.path = path
				self.status_bar.SetStatusText(CHECKING_THE_DRIVE)
				first_line, second_line, last_line = dir_obj.get_drive_info()
				label_num.SetLabelText(f"{first_line}\r\n{second_line}\r\n{last_line}")
				if self.dir1.path and self.dir2.path:
					if self.dir1.drive == self.dir2.drive:
						self.status_bar.SetStatusText(SAME_DRIVE)
					else:
						self.status_bar.SetStatusText(ALL_SET)
						self.move_button.Enable()
				else:
					self.status_bar.SetStatusText(SELECT_BOTH_DIRS)

	def move_between_two_dirs(self, dir_obj1, dir_obj2):
		if dir_obj1.done and dir_obj2.done:
			return
		try:
			while dir_obj1.fileset:
				file = dir_obj1.fileset.pop()
				from_file = os.path.join(dir_obj1.path, file)
				to_file = os.path.join(dir_obj2.full_output_dir, file)
				print("moving file %s to %s" % (from_file, to_file))
				dir_obj1.files_done_num += 1
				if dir_obj1.number < dir_obj2.number:
					dir1_str = f"Диск {dir_obj1.drive[:-1]} ({dir_obj1.files_done_num}/{dir_obj1.files_num})"
					dir2_str = f"Диск {dir_obj2.drive[:-1]} ({dir_obj2.files_done_num}/{dir_obj2.files_num})"
				else:
					dir2_str = f"Диск {dir_obj1.drive[:-1]} ({dir_obj1.files_done_num}/{dir_obj1.files_num})"
					dir1_str = f"Диск {dir_obj2.drive[:-1]} ({dir_obj2.files_done_num}/{dir_obj2.files_num})"
				progress_str = f"{IN_PROGRESS} {dir1_str}, {dir2_str}"
				print(progress_str)
				self.status_bar.SetStatusText(progress_str)
				try:
					os.makedirs(os.path.dirname(to_file), exist_ok=True)
					shutil.move(from_file, to_file)
				except OSError as e:
					os.remove(to_file)
					dir_obj1.fileset.append(file)
					dir_obj1.files_done_num -= 1
					if e.errno == 28:
						print("out of space, changing direction")
						self.move_between_two_dirs(dir_obj2, dir_obj1)
					else:
						raise e
			dir_obj1.done = True
			self.move_between_two_dirs(dir_obj2, dir_obj1)

		except Exception as e:
			self.status_bar.SetStatusText(ERROR_STATUS)
			dial = wx.MessageDialog(caption=ERROR_MESSAGE, message=repr(e), parent=self.parent, style=wx.ICON_ERROR)
			dial.ShowModal()

	def move_dat_shit(self):
		self.dir1.full_output_dir = os.path.join(os.path.join(self.dir1.path, os.pardir), self.dir2.output_dir)
		self.dir2.full_output_dir = os.path.join(os.path.join(self.dir2.path, os.pardir), self.dir1.output_dir)

		self.move_between_two_dirs(self.dir1, self.dir2)

		for root in [self.dir1.path, self.dir2.path]:
			dirs = list(os.walk(root))[1:]
			dirs.sort(key=lambda e: len(e[0]), reverse=True)
			print(dirs)
			for directory in dirs:
				if not os.listdir(directory[0]):
					os.rmdir(directory[0])
			if not os.listdir(root):
				os.rmdir(root)

	def on_move_pressed(self, event):
		self.move_button.Disable()
		self.select_dir1_button.Disable()
		self.select_dir2_button.Disable()
		self.status_bar.SetStatusText(IN_PROGRESS)
		worker = MovingThread(self, self.dir1, self.dir2)
		worker.start()

	def on_moved(self, event):
		self.move_button.Enable()
		self.select_dir1_button.Enable()
		self.select_dir2_button.Enable()
		print("Done.")
		self.status_bar.SetStatusText("Готово!")

	def __init__(self, parent):
		super(MovePanel, self).__init__(parent)
		self.parent = parent

		self.dir1 = Directory(1)
		self.dir2 = Directory(2)

		self.status_bar = parent.status_bar

		first_group_sizer = wx.BoxSizer(wx.HORIZONTAL)
		self.select_dir1_button = wx.Button(self, label=CHOOSE_DIR1_BUTTON)
		self.label1 = wx.StaticText(self, label="\r\n\r\n", style=wx.ST_NO_AUTORESIZE)
		self.select_dir1_button.Bind(
			wx.EVT_BUTTON,
			lambda evt, dir_obj=self.dir1, label_num=self.label1: self.select_dir(evt, dir_obj, label_num)
		)
		first_group_sizer.Add(self.select_dir1_button, 1, wx.EXPAND)
		first_group_sizer.Add(self.label1, 3, wx.ALL, 10)

		second_group_sizer = wx.BoxSizer(wx.HORIZONTAL)
		self.select_dir2_button = wx.Button(self, label=CHOOSE_DIR2_BUTTON)
		self.label2 = wx.StaticText(self, label="\r\n\r\n", style=wx.ST_NO_AUTORESIZE)
		self.select_dir2_button.Bind(
			wx.EVT_BUTTON,
			lambda evt, dir_obj=self.dir2, label_num=self.label2: self.select_dir(evt, dir_obj, label_num)
		)
		second_group_sizer.Add(self.select_dir2_button, 1, wx.EXPAND)
		second_group_sizer.Add(self.label2, 3, wx.ALL, 10)

		self.move_button = wx.Button(self, label=SWITCH_BUTTON)
		self.move_button.Bind(wx.EVT_BUTTON, self.on_move_pressed)
		self.move_button.Disable()

		self.Bind(FILES_MOVED, self.on_moved)

		main_sizer = wx.BoxSizer(wx.VERTICAL)
		main_sizer.Add(first_group_sizer, 1, wx.EXPAND)
		main_sizer.Add(second_group_sizer, 1, wx.EXPAND)
		main_sizer.Add(self.move_button, 1, wx.EXPAND)

		image_sizer = wx.BoxSizer(wx.HORIZONTAL)
		image = resource_path('assets/image1.png')
		self.imageCtrl = wx.StaticBitmap(self, wx.ID_ANY, wx.Bitmap(image))
		image_sizer.Add(self.imageCtrl, 2, wx.EXPAND)
		image_sizer.Add(main_sizer, 6, wx.EXPAND)
		self.SetSizer(image_sizer)


class MainFrame(wx.Frame):

	def __init__(self):
		wx.Frame.__init__(self, None, title=APP_TITLE,
		                  style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER ^ wx.MAXIMIZE_BOX)
		icon = resource_path('assets/favicon_new.ico')
		self.SetIcon(wx.Icon(icon))
		self.SetSize(wx.Size(800, -1))

		self.status_bar = self.CreateStatusBar(1)
		self.status_bar.SetStatusText(CHOOSE_DIRS)
		self.panel = MovePanel(self)


def resource_path(relative_path):
	""" Get absolute path to resource, works for dev and for PyInstaller """
	try:
		# PyInstaller creates a temp folder and stores path in _MEIPASS
		base_path = sys._MEIPASS
	except Exception:
		base_path = os.path.abspath(".")

	return os.path.join(base_path, relative_path)


if __name__ == "__main__":
	file_moved_event_type = wx.NewEventType()
	FILES_MOVED = wx.PyEventBinder(file_moved_event_type, 1)
	app = wx.App()
	frame = MainFrame()
	frame.Show()
	app.MainLoop()
