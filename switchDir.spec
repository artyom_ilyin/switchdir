# -*- mode: python -*-

block_cipher = None


a = Analysis(['switchDir.py'],
             pathex=['D:\\dev\\switchDir'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

a.datas += [('assets/image1.png','D:\\dev\\switchDir\\assets\\image1.png', 'Data')]
a.datas += [('assets/favicon_new.ico','D:\\dev\\switchDir\\assets\\favicon_new.ico', 'Data')]

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='Ultra Mega Moving Machine Ultimate Extended Edition ArkhiPro',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False , icon='assets\\favicon_new.ico')
